//SOAL 1
const hitungPersegi = (p, l) => {

    //Hitung Luas
    let luas
    luas = p*l

    //Hitung Keliling
    let keliling
    keliling = 2*(p+l)

    return "Luas persegi : " + luas + "," + " Keliling Persegi : "+ keliling
}

console.log(hitungPersegi(2,5))


//SOAL 2
const newFunction = (firstName, lastName) => {
    return console.log(`${firstName} ${lastName}`)
    }
  //Driver Code 
  newFunction("William", "Imoh")

  
//SOAL 3
var newObject = {
    firstName: 'Fidri',
    lastName: 'Lazuardi',
    address: 'Sukabumi',
    hobby: 'Sepakbola'
};

const {firstName, lastName, address, hobby} = newObject
console.log(newObject)


//SOAL 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)


//SOAL 5
const planet = "earth" 
const view = "glass" 
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit,${planet}`
console.log(before)