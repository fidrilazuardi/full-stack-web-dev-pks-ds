<?php
trait Hewan {
    public $nama;
    public $darah=50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi() {
        echo $this->nama .  'sedang' . $this->keahlian . '<br>';
    }
}

trait fight
{
    public $attackPower;
    public $defensePower;

    public function serang($diserang)
    {
        echo $this->nama . ' sedang menyerang ' . $diserang .'<br>';
    }

    public function diserang($attackPenyerang)
    {
        $darah = $this->darah;

        $darahSekarang  = $darah - ($attackPenyerang / $this->defencePower);
        $this->darah = $darahSekarang;

        echo $this->nama . ' sedang diserang <br> Darah ' . $this->nama . ' sekarang ' . $darahSekarang;
    }
}


class Harimau {
    use Hewan, Fight;

    function __construct($nama) {
        $this->nama = $nama;
        $this->jumlahKaki=4;
        $this->keahlian='Lari Cepat';
        $this->attackPower=7;
        $this->defencePower=8;
      }

     function getInfoHewan() {
        echo 'Jenis = Harimau';
        echo '<br>';
        echo 'Nama =' . $this->nama;
        echo '<br>';
        echo 'Darah =' . $this->darah;
        echo '<br>';
        echo 'Jumlah Kaki =' . $this->jumlahKaki;
        echo '<br>';
        echo 'Keahlian =' . $this->keahlian;
        echo '<br>';
        echo 'Attack Power =' . $this->attackPower;
        echo '<br>';
        echo 'Defence Power =' . $this->defencePower;
        echo '<br>';
      }
}

class Elang {
    use Hewan, Fight;

    function __construct($nama) {
        $this->nama = $nama;
        $this->jumlahKaki=2;
        $this->keahlian='Terbang Tinggi';
        $this->attackPower=10;
        $this->defencePower=5;
      }

      function getInfoHewan() {
        echo 'Jenis = Elang';
        echo '<br>';
        echo 'Nama =' . $this->nama;
        echo '<br>';
        echo 'Darah =' . $this->darah;
        echo '<br>';
        echo 'Jumlah Kaki =' . $this->jumlahKaki;
        echo '<br>';
        echo 'Keahlian =' . $this->keahlian;
        echo '<br>';
        echo 'Attack Power =' . $this->attackPower;
        echo '<br>';
        echo 'Defence Power =' . $this->defencePower;
        echo '<br>';
      }
}

$harimau = new Harimau('harimau_1');
$elang = new Elang('elang_1');
$harimau->getInfoHewan();
echo '<br>';
$elang->getInfoHewan();
echo '<hr>';
$elang->atraksi();
$elang->serang($harimau->nama);
$harimau->diserang($elang->attackPower);
echo '<hr>';
$harimau->atraksi();
$harimau->serang($elang->nama);
$elang->diserang($harimau->attackPower);
echo '<hr>';
$harimau->getInfoHewan();
echo '<br>';
$elang->getInfoHewan();
?>