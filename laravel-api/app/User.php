<?php

namespace App;

use App\Role;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'username', 'role_id', 'password', 'email_verified_at'];
    
    protected $keyType = 'string' ;

    public $incrementing = false;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating( function ($model){
            if(empty( $model->id) ){
                $model->id = Str::uuid();
            }

            $model->role_id = Role::where('name', 'author')->first()->id;
        });
    }

    public function roles(){
        return $this->belongsToMany('App\Roles');
    }

    public function otp_code()
    {
        return $this->hasOne('App\OtpCode');
    }

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    public function otp_codes(){
        return $this->hasOne('App\OtpCode');
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }

    public function post(){
        return $this->hasMany('App\Post');
    }

}

// class Users extends Model
// {
//     protected $fillable = ['id' , 'username', 'email', 'name', 'roles_id'];

//     protected $keyType = 'string' ;

//     public $incrementing = false;

    
// }

