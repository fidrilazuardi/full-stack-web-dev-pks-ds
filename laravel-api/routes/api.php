<?php

use Illuminate\Http\Request;
use App\Http\Controllers\Auth\RegisterController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * route resource post
 */
Route::apiResource('/post', 'PostController');
Route::apiResource('/role', 'RoleController');
Route::apiResource('/comment', 'CommentController');

Route::group([
    'prefix'=>'auth' ,
    'namespace'=>'Auth'
], function(){
    Route::post('register', 'RegisterController')->name('auth.register');
    Route::post('regenarate-otp-code', 'RegenerateOtpCodeController')->name('auth.regenarate_otp_code');
    Route::post('verification', 'VerificationController')->name('auth.verification');
    Route::post('update-password', 'UpdatePasswordController')->name('auth.update_password');
    Route::post('login', 'LoginController')->name('auth.login');
});

Route::get('/test', function(){
    return 'Masuk Pak Eko';
})->middleware('auth:api');
