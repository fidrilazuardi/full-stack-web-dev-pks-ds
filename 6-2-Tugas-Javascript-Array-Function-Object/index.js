//Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"]
var cek=1

for (let i = 0; i < daftarHewan.length; i++) {
    
    for (let j = 0; j < daftarHewan.length; j++) {
        var cekHewan = daftarHewan[j]
        if (cek == cekHewan.substring(0, 1)) {
            cek++
            console.log(daftarHewan[j])
        }
        
        
    }
}

//SOal 2
function introduce(data){
return "Nama saya " + data.name + ", umur saya "+ data.age + " tahun, alamat saya di " + data.address + ", dan saya punya hobby yaitu "+ data.hobby
}
 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"

//Soal 3
function hitung_huruf_vokal(name){
    var vokal = 'aeiouAEIOU';
	var count = 0;

	for (var x = 0; x < name.length; x++) {
		if (vokal.indexOf(name[x]) !== -1) {
			count++;
		}
	}
	return count;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

//Soal 4
function hitung(angka){
    var i = -2
    return (angka * 2) + i
}


console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8